{
  network.description = "my page";
  webserver = {config, pkgs, ...}:
  let
    mywebpage = import ./default.nix { inherit pkgs; };
  in

  {
    networking.firewall.allowedTCPPorts = [ 80 ];

    systemd.services.mywebpage = {
      wantedBy = [ "multi-user.target" ] ;
      after = [ "network.target" ];
      script = "PORT=80 ${mywebpage}/bin/mywebpage-server"; };

    deployment = {
      targetEnv = "virtualbox";
      virtualbox = {
        memorySize = 512; 
        vcpu = 1; 
        headless = true;
     };
  };
};
}

