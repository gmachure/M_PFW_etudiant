{ pkgs ? import <nixpkgs> {} } :
with pkgs;
stdenv.mkDerivation {
  name = "mywebpage";
  src = ./.;
  installPhase = ''
    mkdir $out
    cp data/index.html $out/
  '';
}
