{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import           System.Environment (getArgs)

main :: IO ()
main = do 
    args <- getArgs
    let arg = head args
    lines <- readFile arg
    (mapM_ putStrLn) lines
