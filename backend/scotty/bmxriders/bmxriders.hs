{-# LANGUAGE OverloadedStrings #-}

import           Clay as C
import           Prelude
import           Control.Monad.Trans (liftIO) 
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import qualified Database.PostgreSQL.Simple as SQL
import           Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import           Lucid
import           Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Network.Wai.Middleware.Static (addBase, staticPolicy)
import qualified Web.Scotty as S

myCss :: C.Css
myCss = do
    C.a C.# C.byClass "aCss" C.? do
        C.textDecoration  C.none
        C.color           C.inherit
    C.body C.? do
        C.backgroundColor  C.azure
    C.div C.# C.byClass "divCss" C.? do
        C.backgroundColor  C.beige
        C.border           C.solid (C.px 1) C.black
        C.margin           (C.em 1) (C.em 1) (C.em 1) (C.em 1)
        C.width            (C.px 320)
        C.textAlign        C.center
        C.float            C.floatLeft
    C.img C.# C.byClass "imgCss" C.? do
        C.width            (C.px 320)
        C.height           (C.px 240)
    C.p C.# C.byClass "pCss" C.? do
        C.fontWeight C.bold

data Bmx = Bmx
    { _name :: T.Text
    , _image:: T.Text
    } deriving Show

instance FromRow Bmx where
    fromRow = Bmx <$> field  <*> field

getCursor:: IO SQL.Connection
getCursor = do
    cursor <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=bmxriders user=bmxrideruser password='bmxrideruser'"
    return cursor

getAllBmx:: SQL.Connection -> IO [Bmx]
getAllBmx cursor = do
    result <- SQL.query_ cursor "SELECT br.name, br.image FROM bmxrider br" :: IO [Bmx]
    return result

bmxToDiv:: Bmx -> Html()
bmxToDiv (Bmx name image) =
    a_ [href_ image] $ do
        div_ [class_ "divCss"] $ do
            p_ [class_ "pCss"] $ toHtml name
            img_ [class_ "imgCss", src_ image]

main :: IO ()
main = do
    
    cursor <- getCursor
    allBmx <- getAllBmx cursor
    
    S.scotty 3000 $ do
        S.middleware logStdoutDev
        S.middleware $ staticPolicy $ addBase "img"
        S.get "/" $ do
            S.html $ renderText $ html_ $ do
                head_ $ do
                    style_ $ L.toStrict $ C.render $ myCss
                    title_ "Bmx riders"
                body_ $ do
                    h1_ "Bmx riders"
                    div_ $ mapM_ bmxToDiv allBmx

    
        
