{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import           Web.Scotty (scotty, get, html, param, rescue, middleware)
import           Lucid

getHomePage = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "The helloscotty"
            a_ [href_ "/hello"] "go to hello page"

getHelloHtml name = do
    doctype_
    html_ $ do
        head_ $ do
            meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ $ toHtml (T.concat ["Hello ", name, " !"])
            form_ [action_ "/hello", method_ "get"] $ do
                input_ [type_ "text", name_ "name"]
                button_ [type_ "submit"] $ "Valider"
            
            a_ [href_ "/"] "go to home page"

main = scotty 3000 $ do
    middleware logStdoutDev
    get "/" $ html $ renderText $ getHomePage

    get "/hello" $ do
        name <- param "name" `rescue` (\_ -> return "")
        html $ renderText $ getHelloHtml name