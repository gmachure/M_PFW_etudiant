{-# LANGUAGE OverloadedStrings #-}

module View (mkpage, homeRoute, newRoute) where

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid

import qualified Model

-- TODO implement CSS styles
-- TODO implement route views

mkpage :: Lucid.Html () -> Lucid.Html () -> L.Text
mkpage titleStr page = renderText $ html_ $ do
  head_ $ do
    title_ titleStr
  body_ page



getLiHtml:: Model.Poem -> Html()
getLiHtml (Model.Poem id author title y body) = 
    li_ $ do
      div_ $ toHtml title
      div_ $ do
        span_ $ toHtml $ "by " ++ show author -- add concatenation title year author
        span_ $ toHtml $ " (" ++ show y ++ ")"

homeRoute :: [Model.Poem] -> Lucid.Html ()
homeRoute poems = do
  h1_ "Poem hub"
  a_ [href_ "/new"] "Create a new poem"
  ul_ $ mapM_ getLiHtml poems

newRoute :: Lucid.Html ()
newRoute = do
  h1_ "Write a new poem"
  form_ [action_ "/", method_ "get"] $ do
    div_ $ do
      label_ [for_ "author"] "author"
      input_ [type_ "text", name_ "author"]
    div_ $ do
      label_ [for_ "title"] "title"
      input_ [type_ "text", name_ "title"]
    div_ $ do
      label_ [for_ "year"] "year"
      input_ [type_ "number", name_ "year"]
    div_ $ do
      label_ [for_ "body"] "body"
      input_ [type_ "textarea", name_ "body"]
    div_ $ do
      button_ [type_ "submit"] $ "Submit"
  button_ [type_ "submit"] $ "Cancel"