{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans (liftIO)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (get, middleware, param, post, rescue, scotty, html)

import qualified Model
import qualified View

-- TODO implement routes

main = do
  scotty 3000 $ do
    middleware logStdoutDev
    get "/" $ do
      id <- Model.getNewId $ liftIO Model.getAllPoems
      title <- param "title" `rescue` (\_ -> return "")
      author <- param "author" `rescue` (\_ -> return "")
      year <- param "year" `rescue` (\_ -> return "")
      body <- param "body" `rescue` (\_ -> return "")
      Model.insertPoem id title author year body
      poems <- liftIO Model.getAllPoems
      html $ View.mkpage "Poem hub - Home" $ View.homeRoute poems 
    get "/new" $ do
      html $ View.mkpage "Poem hub - New" $ View.newRoute 