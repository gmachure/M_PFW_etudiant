{-# LANGUAGE OverloadedStrings #-}

module Model where

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import           Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

dbName = "poemhub.db"

data Poem = Poem 
  { id :: Int
  , author :: T.Text
  , title :: T.Text
  , year :: Int
  , body :: T.Text 
  } deriving Show

instance FromRow Poem where
  fromRow = Poem <$> field <*> field <*> field <*> field <*> field

getYearText :: Poem -> T.Text
getYearText = T.pack . show . Model.year
 

getIdText :: Poem -> T.Text
getIdText = T.pack . show . Model.id

getAllPoems :: IO [Poem]
getAllPoems = do
  conn <- SQL.open dbName
  res <- SQL.query_ conn "SELECT p.id, p.author, p.title, p.year, p.body FROM poems p" :: IO [Poem]
  SQL.close conn
  return res

mymax :: [Int] -> Int
mymax (h : []) =
  h

mymax (h : t) =
  



getNewId :: [Poem] -> Int
getNewId poems = do
  ids <- map (\p -> p) [1, 2, 3]--Model.id) [1, 2, 3]-- [poems]
  res <- maximum ids 
  return res

insertPoem :: Int -> String -> String -> Int -> String
insertPoem id title author year body= do
  conn <- SQL.open dbName
  SQL.execute conn
  "INSERT INTO mytable VALUES (?, ?, ?, ?, ?)"
  (id::Int, T.pack title, T.pack author, year::Int, T.pack body)