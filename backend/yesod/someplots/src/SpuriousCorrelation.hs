module SpuriousCorrelation where

data Data = Data {
  year :: Int,
  sociologyPhd :: Int,
  spaceLaunches :: Int }

allData = [
  Data 1997 601 54, 
  Data 1998 579 46, 
  Data 1999 572 42, 
  Data 2000 617 50, 
  Data 2001 566 43, 
  Data 2002 547 41, 
  Data 2003 597 46, 
  Data 2004 580 39, 
  Data 2005 536 37, 
  Data 2006 579 45, 
  Data 2007 576 45, 
  Data 2008 601 41, 
  Data 2009 664 54]

