{-# LANGUAGE OverloadedStrings #-}

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import Data.List.Split
import           Lucid
import           System.Environment (getArgs, getProgName)

myPage :: [String] -> Html ()
myPage lis = do
  doctype_
  html_ $ do
    head_ $ do
      meta_ [charset_ "utf-8"]
    body_ $ do
      h1_ "my page"
      ul_ $ toHtml $ concat lis

getLis :: [String] -> [String]
getLis [] = []

getLis (h:t) = (li_ h) ++ getLis t

main :: IO ()
main = do
    args <- getArgs
    if length args /= 2
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <input dat> <output html>"
    else do
        let [input, output] = args
        let file = readFile input
        putStrLn file
        -- let lines = splitOn "\n" file 
        -- let lis = getLis lines
        -- renderToFile output $ myPage lis
