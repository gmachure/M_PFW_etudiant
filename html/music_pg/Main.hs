{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='localhost' port='5432' dbname='mydb' user='toto' password='toto'"

    res_1 <- SQL.query_ conn "SELECT id, name from artists" :: IO [(Int, T.Text)]
    mapM_ print res_1
    -- TODO
    

    putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
    res_2 <- SQL.query conn "SELECT id, name from artists where name=?" (SQL.Only $ T.pack "Radiohead") :: IO [(Int, T.Text)]
    mapM_ print res_2
    -- TODO

    putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
    res_3 <- SQL.query conn "SELECT * FROM titles where name LIKE ? AND id > ?" ("%ust%"::T.Text, 1::Int) :: IO [(Int, Int, T.Text)]
    mapM_ print res_3
    -- TODO

    putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
    res_4 <- SQL.query_ conn "SELECT name FROM titles" :: IO [Name]
    mapM_ print res_4
    -- TODO

    putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
    res_5 <- SQL.query_ conn "SELECT name FROM titles" :: IO [[T.Text]]
    mapM_ print res_5
    -- TODO

    SQL.close conn

