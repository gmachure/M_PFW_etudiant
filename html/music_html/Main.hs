{-# LANGUAGE OverloadedStrings #-}

import qualified Clay as C
import           Lucid

import qualified Data.Text as T
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)

data Music = Music
  { _title :: T.Text
  , _artist:: T.Text
  } deriving Show

instance FromRow Music where
  fromRow = Music <$> field  <*> field
  
getStringFromMusic:: Music -> String
getStringFromMusic (Music _title _artist) = concat [T.unpack _title, " - ", T.unpack _artist]

-- getLiHtml [] = []
getLiHtml list = (getStringFromMusic $ li_ $ head list):(getLiHtml $ tail list)



getMusics:: SQL.Connection -> IO [Music]
getMusics cursor = do 
  result <- SQL.query_ cursor "SELECT t.name, a.name FROM artists a, titles t WHERE t.artist = a.id;"  :: IO [Music]
  return result


getCursor:: IO SQL.Connection
getCursor = do
  cursor <- SQL.connectPostgreSQL "host='localhost' port='5432' dbname='mydb' user='toto' password='toto'"
  return cursor


myPage :: [String] -> Html ()
myPage lis = do
  doctype_
  html_ $ do
    head_ $ do
      meta_ [charset_ "utf-8"]
    body_ $ do
      h1_ "my page"
      ul_ $ toHtml $ concat lis

main :: IO ()
main = do
    cursor <- getCursor

    res_1 <- getMusics cursor
    mapM_ print res_1
  

    SQL.close cursor


    
