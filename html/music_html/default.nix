{ pkgs ? import <nixpkgs> {} }:
let
  drv = pkgs.haskellPackages.callCabal2nix "music-html" ./. {};
in
if pkgs.lib.inNixShell then drv.env else drv
