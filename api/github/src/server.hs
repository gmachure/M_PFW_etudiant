{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import Common
import User

import Data.Maybe
import qualified Data.Text as T
import Network.Wai.Handler.Warp (run)
import Servant

myUsers :: [User]
myUsers = 
    [ User 2 "spongebob"
    , User 1 "johndoe"
    , User 171292423 "juliendehos"
    ]

findUser :: [User] -> T.Text -> Maybe User
findUser users userName = listToMaybe $ filter ((==) userName . login) users

handleUser :: T.Text -> h -> Handler (Maybe User)
handleUser userName _ = return (findUser myUsers userName)

server :: Server MyhubApi
server = handleUser

main :: IO ()
main = run 3000 $ serve (Proxy @MyhubApi) server

